import java.util.concurrent.atomic.AtomicInteger;

public class Observer extends Thread {

    private Bank _bank;
    private static final int PAUSE = 300;
    private static final int MIN_BALANCE = 500;
    private static final int MAX_BALANCE = 1500;
    private static final int BALANCE_TO_OR_FROM_CASHIER  = 500;
    private boolean isActive;

    public Observer(Bank bank) {
        isActive = true;
        _bank = bank;
    }

    public void ShowTransaction(Cashier cashier, OperationType operation, BalanceType balance) {
        if (operation == OperationType.WITHDRAW) {
            System.out.println("\u001B[32m" + "Cashier (id " + cashier.get_id() + ") - "
                    + operation.toString() + " (" + balance.toString() + ")" + "\u001B[0m");
        }
        if (operation == OperationType.REPLENISH) {
            System.out.println("\u001B[31m" + "Cashier (id " + cashier.get_id() + ") - "
                    + operation.toString() + " (" + balance.toString() + ")" + "\u001B[0m");
        }
    }

    public void Disable(){
        isActive = false;
    }

    @Override
    public void run() {

        try {
            while (isActive) {

                for(int i = 0; i < _bank.NUMBER_CASHIERS; i++){
                    var cashier = _bank.getCashier();

                    try {
                        if (cashier.get_account().get_balanceUAH() < MIN_BALANCE) {

                            _bank.get_storageUAH().addAndGet(-1 * BALANCE_TO_OR_FROM_CASHIER);
                            cashier.get_account().set_balanceUAH(cashier.get_account().get_balanceUAH() + BALANCE_TO_OR_FROM_CASHIER);
                            ShowTransaction(cashier, OperationType.REPLENISH, BalanceType.UAH);
                        }
                        if (cashier.get_account().get_balanceUAH() > MAX_BALANCE) {

                            cashier.get_account().set_balanceUAH(cashier.get_account().get_balanceUAH() - BALANCE_TO_OR_FROM_CASHIER);
                            _bank.get_storageUAH().addAndGet(BALANCE_TO_OR_FROM_CASHIER);
                            ShowTransaction(cashier, OperationType.WITHDRAW, BalanceType.UAH);
                        }
                        if(cashier.get_account().get_balanceUSD() < MIN_BALANCE)
                        {

                            _bank.get_storageUSD().addAndGet(-1 * BALANCE_TO_OR_FROM_CASHIER);
                            cashier.get_account().set_balanceUSD(cashier.get_account().get_balanceUSD() + BALANCE_TO_OR_FROM_CASHIER);
                            ShowTransaction(cashier, OperationType.REPLENISH, BalanceType.USD);
                        }
                        if (cashier.get_account().get_balanceUSD() > MAX_BALANCE) {

                            cashier.get_account().set_balanceUSD(cashier.get_account().get_balanceUSD() - BALANCE_TO_OR_FROM_CASHIER);
                            _bank.get_storageUSD().addAndGet(BALANCE_TO_OR_FROM_CASHIER);
                            ShowTransaction(cashier, OperationType.WITHDRAW, BalanceType.USD);
                        }
                        if(_bank.get_storageUAH() == new AtomicInteger(0)
                                || _bank.get_storageUSD() == new AtomicInteger(0)){
                            throw new Exception("One from storage is empty");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }finally {
                        _bank.returnCashier(cashier);
                    }
                }

                Thread.sleep(PAUSE);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
