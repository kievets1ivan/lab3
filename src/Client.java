import java.util.Random;

public class Client extends Thread {

    private int _id;
    private Account _account;
    private Bank _bank;
    private int _numberOfOperation;

    public Client(Bank bank, int id, int numberOfOperation) {
        _account = new Account(AccountType.CLIENT);
        _bank = bank;
        _id = id;
        _numberOfOperation = numberOfOperation;
    }

    public int get_id() {
        return _id;
    }

    @Override
    public void run() {

        Cashier cashier = null;
        int tempMoney = 0;
        BalanceType tempBalance = null;
        OperationType tempOperation = null;
        Client toClient = null;

        while (_numberOfOperation != 0) {
            try {

                cashier = _bank.getCashier();

                tempOperation = GetRandomOperation();

                switch (tempOperation) {
                    case REPLENISH:

                        tempBalance = GetRandomBalance();
                        tempMoney = new Random().nextInt(351) + 50;


                        cashier.Replenish(_account, tempBalance, tempMoney);
                        ShowTransaction(tempOperation, tempBalance, tempMoney);
                        break;
                    case WITHDRAW:

                        tempBalance = GetRandomBalance();
                        tempMoney = new Random().nextInt(251) + 50;


                        cashier.Withdraw(_account, tempBalance, tempMoney);

                        ShowTransaction(tempOperation, tempBalance, tempMoney);


                        break;
                    case EXCHANGE:

                        tempBalance = GetRandomBalance();

                        if (tempBalance == BalanceType.UAH) {
                            tempMoney = new Random().nextInt(351) + 50;
                        }
                        if (tempBalance == BalanceType.USD) {
                            tempMoney = new Random().nextInt(26) + 5;
                        }


                        cashier.Exchange(_account, tempBalance, tempMoney, _bank.get_exchangeRates());
                        ShowTransaction(tempOperation, tempBalance, tempMoney);

                        break;

                    case TRANSFER:
                        tempBalance = GetRandomBalance();
                        tempMoney = new Random().nextInt(351) + 50;

                        while (true) {
                            toClient = _bank.getClient();

                            if (toClient._id != this._id) {
                                break;
                            }
                            _bank.returnClient(toClient);
                        }


                        cashier.Transfer(_account, toClient._account, tempBalance, tempMoney);

                        ShowTransaction(this, toClient, tempOperation, tempBalance, tempMoney);

                        _bank.returnClient(toClient);
                        break;
                }

            } catch (OperationException e) {

                WriteFailedTransactionToLog(this, toClient, tempOperation, tempBalance, tempMoney, e);

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                try {
                    _bank.returnCashier(cashier);
                    _numberOfOperation--;
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public BalanceType GetRandomBalance() {
        var random = new Random().nextInt(2) + 1;

        switch (random) {
            case 1:
                return BalanceType.UAH;
            case 2:
                return BalanceType.USD;
        }
        return null;
    }

    public OperationType GetRandomOperation() {
        var random = new Random().nextInt(4) + 1;

        switch (random) {
            case 1:
                return OperationType.REPLENISH;
            case 2:
                return OperationType.WITHDRAW;
            case 3:
                return OperationType.EXCHANGE;
            case 4:
                return OperationType.TRANSFER;
        }
        return null;
    }

    public void ShowTransaction(OperationType operation, BalanceType balance, int money) {
        var record = "Client (id " + _id + ") - " + operation.toString() + " " + money + " (" + balance.toString() + " balance)";
        WriteTransactionToLog(record);
        System.out.println(record);
    }

    public void ShowTransaction(Client fromClient,
                                Client toClient,
                                OperationType operation,
                                BalanceType balance,
                                int money) {
        var record = "From client (id " + fromClient._id + ") to client (id " + toClient._id + ") - "
                + operation.toString() + " " + money + " (" + balance.toString() + " balance)";

        WriteTransactionToLog(record);
        System.out.println(record);
    }

    private void WriteFailedTransactionToLog(Client fromClient,
                                             Client toClient,
                                             OperationType operation,
                                             BalanceType balance,
                                             int money,
                                             OperationException e) {
        String record;
        if (operation == OperationType.TRANSFER) {
            record = "From client (id " + fromClient._id + ") to client (id " + toClient._id + ") - "
                    + operation.toString() + " " + money + " (" + balance.toString() + " balance)";
        } else {
            record = "Client (id " + _id + ") - " + operation.toString() + " " + money + " (" + balance.toString() + " balance)";
        }

        _bank.registerTransaction(this._id, record + "\u001B[31m" + " (Transaction failed - "+ e.accountType.toString() +")" + "\u001B[0m");

    }

    private void WriteTransactionToLog(String record) {
        _bank.registerTransaction(this._id, record);
    }

}
