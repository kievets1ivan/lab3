import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Bank {

    public static final int NUMBER_CASHIERS = 3;
    private AtomicInteger _storageUAH = new AtomicInteger(5000);
    private AtomicInteger _storageUSD = new AtomicInteger(5000);
    public final BlockingQueue<Cashier> Cashiers = new ArrayBlockingQueue<>(NUMBER_CASHIERS);
    public final BlockingQueue<Client> Clients;
    public TransactionLog Transactions;

    private int _exchangeRates = 27;

    public Bank(int numberOfClients) {
        Cashiers.add(new Cashier());
        Cashiers.add(new Cashier());
        Cashiers.add(new Cashier());
        Clients = new ArrayBlockingQueue<>(numberOfClients);
        Transactions = new TransactionLog();
    }

    public AtomicInteger get_storageUAH() {
        return _storageUAH;
    }

    public AtomicInteger get_storageUSD() {
        return _storageUSD;
    }

    public int get_exchangeRates() {
        return _exchangeRates;
    }

    public Client getClient() throws InterruptedException {
        return Clients.take();
    }

    public void returnClient(Client client) throws InterruptedException {
        Clients.put(client);
    }

    public void registerClient(Client client) throws InterruptedException {
        Clients.put(client);
        Transactions.AddClient(client.get_id());
    }

    public Cashier getCashier() throws InterruptedException {
        return Cashiers.take();
    }

    public void returnCashier(Cashier cashier) throws InterruptedException {
        Cashiers.put(cashier);
    }

    public void registerTransaction(int id, String record){
        Transactions.AddRecordToLogById(id, record);
    }

    public void ShowCashierBalances() {

        for(int i = 0; i < NUMBER_CASHIERS; i++){
            try {
                Cashiers.take().GetCashierBalances();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void ShowClientsTransactionLogs() {

        for ( int key : Transactions.GetKeySet() ) {
            System.out.println("\u001B[35m" + "Client (id " + key + ") transaction log:" + "\u001B[0m");

            Transactions.RetrieveLogById(key).forEach(transaction -> System.out.println(transaction));
        }
    }

    public void ShowStorageBalances(){
        System.out.println("Bank storage:");

        System.out.println("UAH: " + _storageUAH);
        System.out.println("USD: " + _storageUSD);
    }
}
