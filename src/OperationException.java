public class OperationException extends Exception {

    public AccountType accountType;

    public OperationException(AccountType accountType) {
        super();
        this.accountType = accountType;
    }
}
