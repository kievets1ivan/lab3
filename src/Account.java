public class Account {

    private double _balanceUAH;
    private double _balanceUSD;
    public AccountType accountType;

    public Account(AccountType accountType) {
        this.accountType = accountType;
        if (accountType == AccountType.CASHIER){
            _balanceUAH = 1000;
            _balanceUSD = 1000;
        }
        if (accountType == AccountType.CLIENT) {
            _balanceUAH = 500;
            _balanceUSD = 500;
        }
    }

    public double get_balanceUAH() {
        return _balanceUAH;
    }

    public double get_balanceUSD() {
        return _balanceUSD;
    }

    public void set_balanceUAH(double balanceUAH) throws Exception {
        try {
            if (balanceUAH < 0)
                throw new OperationException(accountType);

            _balanceUAH = balanceUAH;

        } catch (OperationException e) {
            throw new OperationException(accountType);

        } catch (Exception e) {
            throw new Exception();
        }
    }

    public void set_balanceUSD(double balanceUSD) throws Exception {
        try {
            if (balanceUSD < 0)
                throw new OperationException(accountType);

            _balanceUSD = balanceUSD;

        } catch (OperationException e) {
            throw new OperationException(accountType);

        } catch (Exception e) {
            throw new Exception();
        }
    }
}

