import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class TransactionLog {

    public HashMap<Integer, Queue<String>> TransactionLog = new HashMap();

    public TransactionLog(){ }

    public void AddClient(int clientId){
        TransactionLog.put(clientId, new LinkedList());
    }

    public void AddRecordToLogById(int id, String record) {

        var list = TransactionLog.get(id);

        list.add(record);

        TransactionLog.put(id, list);
    }

    public Queue<String> RetrieveLogById(int id){
        return TransactionLog.get(id);
    }

    public Set<Integer> GetKeySet(){
        return TransactionLog.keySet();
    }
}