enum AccountType {
    CLIENT,
    CASHIER
}

enum OperationType {
    REPLENISH,
    WITHDRAW,
    TRANSFER,
    EXCHANGE
}

enum BalanceType {
    UAH,
    USD
}
