public class Cashier {

    private Account _account;
    private static int CounterId = 1;
    private int _id;

    public Account get_account() {
        return _account;
    }

    public int get_id() {
        return _id;
    }

    public Cashier() {
        _account = new Account(AccountType.CASHIER);
        _id = CounterId++;
    }

    public void GetCashierBalances() {
        String formattedDoubleUAH = String.format("%.3f", _account.get_balanceUAH());
        String formattedDoubleUSD = String.format("%.3f", _account.get_balanceUSD());

        System.out.println("\u001B[35m" + "Cashier (id " + _id + ") balances: " +  "\u001B[0m");
        System.out.println("UAH: " + formattedDoubleUAH);
        System.out.println("USD: " + formattedDoubleUSD);
    }

    public void Replenish(Account clientAccount,
                          BalanceType balance,
                          int money) throws Exception {

        switch (balance) {
            case UAH:

                _account.set_balanceUAH(_account.get_balanceUAH() + money);
                clientAccount.set_balanceUAH(clientAccount.get_balanceUAH() + money);

                break;
            case USD:

                _account.set_balanceUSD(_account.get_balanceUSD() + money);
                clientAccount.set_balanceUSD(clientAccount.get_balanceUSD() + money);

                break;
        }
    }

    public void Withdraw(Account clientAccount,
                         BalanceType balance,
                         int money) throws Exception {
        try {

            switch (balance) {
                case UAH:

                    clientAccount.set_balanceUAH(clientAccount.get_balanceUAH() - money);
                    _account.set_balanceUAH(_account.get_balanceUAH() - money);

                    break;
                case USD:

                    clientAccount.set_balanceUSD(clientAccount.get_balanceUSD() - money);
                    _account.set_balanceUSD(_account.get_balanceUSD() - money);

                    break;
            }
        }catch (OperationException e){
            throw new OperationException(e.accountType);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception();
        }
    }

    public void Transfer(Account fromClientAccount,
                         Account toClientAccount,
                         BalanceType balance,
                         int money) throws Exception {

        try {

            switch (balance) {
                case UAH:

                    fromClientAccount.set_balanceUAH(fromClientAccount.get_balanceUAH() - money);
                    toClientAccount.set_balanceUAH(toClientAccount.get_balanceUAH() + money);

                    break;
                case USD:

                    fromClientAccount.set_balanceUSD(fromClientAccount.get_balanceUSD() - money);
                    toClientAccount.set_balanceUSD(toClientAccount.get_balanceUSD() + money);

                    break;
            }
        } catch (OperationException e){
            throw new OperationException(e.accountType);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception();
        }
    }


    public void Exchange(Account clientAccount,
                         BalanceType balance,
                         int money,
                         int exchangeRate) throws Exception {

        try {
            switch (balance) {
                case UAH:

                    clientAccount.set_balanceUAH(clientAccount.get_balanceUAH() - money);
                    _account.set_balanceUAH(_account.get_balanceUAH() + money);
                    _account.set_balanceUSD(_account.get_balanceUSD() - (double) (money) / (double) exchangeRate);
                    clientAccount.set_balanceUSD(clientAccount.get_balanceUSD() + (double) (money) / (double) exchangeRate);

                    break;
                case USD:

                    clientAccount.set_balanceUSD(clientAccount.get_balanceUSD() - money);
                    _account.set_balanceUSD(_account.get_balanceUSD() + money);
                    _account.set_balanceUAH(_account.get_balanceUAH() - money * exchangeRate);
                    clientAccount.set_balanceUAH(clientAccount.get_balanceUAH() + money * exchangeRate);

                    break;
            }
        } catch (OperationException e){
            throw new OperationException(e.accountType);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception();
        }
    }
}
