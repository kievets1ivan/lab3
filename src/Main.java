import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        // write your code here

        var numberOfClients = InputNumberOfClients();
        var numberOfOperationsPerClients = InputNumberOfOperationsPerClient();

        var bank = new Bank(numberOfClients);
        var observer = new Observer(bank);

        observer.start();

        for (int i = 1; i <= numberOfClients; i++) {
            var client = new Client(bank, i, numberOfOperationsPerClients);
            bank.registerClient(client);

            client.start();
        }

        Thread.sleep( (long) Math.ceil((double)numberOfOperationsPerClients/(double)5) * 1500 + 1000 );

        observer.Disable();

        bank.ShowStorageBalances();
        bank.ShowCashierBalances();
        bank.ShowClientsTransactionLogs();

    }

    public static int InputNumberOfClients() {
        int number = 0;

        while (true) {
            System.out.print("Enter number of clients: ");
            try {
                number = Integer.parseInt(new Scanner(System.in).nextLine());
                if (number < 2) {
                    throw new Exception();
                }
                break;
            } catch (Exception e) {
                System.out.println("Invalid number");
            }
        }

        return number;
    }

    public static int InputNumberOfOperationsPerClient() {
        int number = 0;

        while (true) {
            System.out.print("Enter number of operation per client: ");

            try {
                number = Integer.parseInt(new Scanner(System.in).nextLine());
                if (number < 1 || number > 15) {
                    throw new Exception();
                }
                break;

            } catch (Exception e) {
                System.out.println("Invalid number");
            }

        }

        return number;
    }
}
